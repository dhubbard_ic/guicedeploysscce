package my.sscce.guice;

import my.sscce.guice.service.MyServiceImpl;
import my.sscce.guice.service.MyService;
import com.google.common.collect.Maps;
import com.google.inject.binder.AnnotatedBindingBuilder;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

import java.util.Map;


public class ApplicationModule extends JerseyServletModule implements Binder {

    @Override
    protected void configureServlets() {

        System.out.println("ApplicationModule.configureServlets");
        
        abstractBind(GuiceContainer.class);

        // Services
        abstractBind(MyService.class).to(MyServiceImpl.class);
        
        bindAuthorizationInterceptor();

        // Bind Routings for main api
        ResourceConfig rc = new PackagesResourceConfig( "my.sscce.guice.route" );
        rc.getClasses().forEach(l -> {
            System.out.println("Binding for ["+l.getName()+"]");
            this.abstractBind(l);
        });

        // Now bind Routings for Service API
        Map<String, String> params = Maps.newHashMap();
        params.put(PackagesResourceConfig.PROPERTY_PACKAGES, "io.swagger.jaxrs.json;io.swagger.jaxrs.listing;my.sscce.guice.route");

        serve( "/*" ).with(GuiceContainer.class, params);
    }

    private void bindAuthorizationInterceptor() {
//       Commented out as not material in deploy problem
    }

    @Override
    public <T> AnnotatedBindingBuilder<T> abstractBind(Class<T> clazz) {
        return bind(clazz);
    }
}
