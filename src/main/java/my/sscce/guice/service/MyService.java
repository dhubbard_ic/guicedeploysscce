package my.sscce.guice.service;

public interface MyService {

    public String process(String userName);
}