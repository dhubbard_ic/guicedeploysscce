/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.sscce.guice.route;

import com.google.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import my.sscce.guice.service.MyService;

@Path("/hello")
public class HelloWorldService {

    private final MyService myService;

    @Inject
    public HelloWorldService(MyService myService) {
       this.myService = myService;
    }
    
    @GET
    @Path("/{userName}")
    public Response getMsg(@PathParam("userName") String userName) {

            String output = myService.process(userName);
            
            return Response.status(200).entity(output).build();

    }

}