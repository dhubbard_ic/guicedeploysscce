package my.sscce.guice;

import com.google.inject.binder.AnnotatedBindingBuilder;

public interface Binder {
    <T> AnnotatedBindingBuilder<T> abstractBind(Class<T> clazz);
}
